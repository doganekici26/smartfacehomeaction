import Page from '@smartface/native/ui/page';
import Button from '@smartface/native/ui/button';
import MaterialTextBox from '@smartface/native/ui/materialtextbox'
import Color from '@smartface/native/ui/color';

export default class Page4 extends Page {
    router: any;
    routeData: any;
    parentController: any;
    constructor() {
      super();
      // Overrides super.onShow method
      this.onShow = function(){
        // tabbar.setSelectedIndex(7,false);
      }.bind(this);
      // Overrides super.onLoad method
      this.onLoad = function(){
            let button = new Button();
            button.backgroundColor = Color.create(Math.floor(Math.random() * 255), Math.floor(Math.random() * 255), Math.floor(Math.random() * 255));
            button.onPress = function() {

            };

            button.height = 100;
            button.width = 100;

            this.layout.addChild(button);

            
                var materialtextbox = new MaterialTextBox({
                    height : 50,
                    hint : "Hint"
                });

                materialtextbox.cursorColor = Color.RED;
                this.layout.addChild(materialtextbox);

        }.bind(this);

    }
  }