import Page from '@smartface/native/ui/page';
import Button from '@smartface/native/ui/button';
import MaterialTextBox from '@smartface/native/ui/materialtextbox'
import Color from '@smartface/native/ui/color';
import ListView from '@smartface/native/ui/listview';
import ListViewItem from '@smartface/native/ui/listviewitem';
import FlexLayout from '@smartface/native/ui/flexlayout';
import Picker from '@smartface/native/ui/picker';

export default class Page5 extends Page {
    router: any;
    routeData: any;
    parentController: any;
    constructor() {
      super();
      // Overrides super.onShow method
      this.onShow = function(){
        // tabbar.setSelectedIndex(7,false);
        //@ts-ignore
        alert("isEmulator :" + SMFApplication.isEmulator());
      }.bind(this);
      // Overrides super.onLoad method
      this.onLoad = function(){
          var self = this;
          self.indexTextObject = {};

        var myListView = new ListView({
            flexGrow:1,
            rowHeight: 100,
            backgroundColor: Color.TRANSPARENT,
            itemCount: 50,
        });

        myListView.onRowCreate = function(){
            var myListViewItem = new ListViewItem();
            var materialtextbox = new MaterialTextBox({
                height : 100,
                hint : "Hint"
                // multiline: true,
                // lineCount: 3
            });
            myListViewItem.addChild(materialtextbox);
            return myListViewItem;
        };
        myListView.onRowBind = function(listViewItem,index){
            var material = listViewItem.getChildList()[0] as MaterialTextBox;
            material.hint = "Hint " + index;
            if (self.indexTextObject[index]) {
                material.text = self.indexTextObject[index];
            } else {
                material.text = "";
            }
            
            if (index == 3 || index == 15) {
                var items = [
                    "item 1",
                    "item 2",
                    "item 3",
                    "item 4",
                    "item 5"
                ];
                var myPicker = new Picker({
                    items: items,
                    currentIndex: 2
                });
                myPicker.onSelected = function(index){
                    material.text = items[index];
                };
                material.ios.inputView = {
                    height: 220,
                    view: myPicker
                }
            } else {
                material.ios.inputView = undefined;
            }

            material.onEditEnds = function(innerIndex) {
                self.indexTextObject[innerIndex] = this.text;
                console.log("innerIndex : " + innerIndex + " Text : " + this.text);
            }.bind(material, index);
        };

        
        this.layout.addChild(myListView);
        }.bind(this);

    }
  }