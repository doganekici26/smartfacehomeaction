import Page from '@smartface/native/ui/page';
import Color from '@smartface/native/ui/color';
import TabBarController from '@smartface/native/ui/tabbarcontroller';
import TabBarItem from '@smartface/native/ui/tabbaritem';
import Button from '@smartface/native/ui/button';

export default class MainSample extends TabBarController {
    tabPages: Page[] = [];
    items: TabBarItem[];
    constructor() {
        //@ts-ignore
        super(this);
        let self = this;
        this.pagingEnabled = true;
        // Overrides super.onShow method
        this.onShow = function() {
            // self.setSelectedIndex(7,false);
        }
        
        // Overrides super.onLoad method
        this.onLoad = onLoad.bind(this);
        this.onPageCreate = onPageCreate.bind(this);
        this.onHide = onHide.bind(this);
        this.onSelected = onSelected.bind(this);
        let recentItem: TabBarItem = new TabBarItem({
            title: "Recent",
            icon: undefined
        });
        let favItem: TabBarItem = new TabBarItem({
            title: "Favorite",
            icon: undefined
        });
        let contactItem: TabBarItem = new TabBarItem({
            title: "Contact",
            icon: undefined
        });
        let messageItem: TabBarItem = new TabBarItem({
            title: "Message",
            icon: undefined
        });

        var items = [];
        var tabPages = [];
        for (let index = 0; index < 10; index++) {
            
            let messageItem: TabBarItem = new TabBarItem({
                title: "Message " + index,
                icon: undefined
            });
            items.push(messageItem)

            
            let page = new Page3(self);
            tabPages.push(page);
        }
        this.items = items;
        this.tabPages = tabPages;
    }
}
function onPageCreate(index: number): Page[] {
    if (index > this.tabPages.length) {
        return [new Page3(this)];
    }
    return this.tabPages[index];
}
function onHide() {
    console.log("hidden");
}
function onSelected(index) {
    console.log("Selected item index: " + index);
};
function onShow() {
    const { headerBar } = this;
}
function onLoad() {
    this.scrollEnabled = true;
    this.indicatorColor = Color.BLACK;
    this.indicatorHeight = 3;
    this.barColor = Color.create("#F3F0F0");
    this.textColor = {
        normal: Color.BLACK,
        selected: Color.create("#00A1F1")
    };
    this.autoCapitalize = true;
}

class Page3 extends Page {
    router: any;
    routeData: any;
    parentController: any;
    constructor(tabbar: TabBarController) {
      super();
      // Overrides super.onShow method
      this.onShow = function(){
        // tabbar.setSelectedIndex(7,false);
      }.bind(this);
      // Overrides super.onLoad method
      this.onLoad = function(){
            let button = new Button();
            button.backgroundColor = Color.create(Math.floor(Math.random() * 255), Math.floor(Math.random() * 255), Math.floor(Math.random() * 255));
            button.onPress = function() {
                tabbar.setSelectedIndex(2,false);
            };

            button.height = 100;
            button.width = 100;

            this.layout.addChild(button)
        }.bind(this);

    }
  }