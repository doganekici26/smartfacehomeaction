/* globals lang */
import 'i18n/i18n'; // Generates global lang object
import Application from '@smartface/native/application';
import { errorStackBySourceMap } from 'error-by-sourcemap';
import System from '@smartface/native/device/system';
import '@smartface/extension-utils';
import 'theme';
import router from 'routes';

// Set uncaught exception handler, all exceptions that are not caught will
// trigger onUnhandledError callback.
Application.onUnhandledError = function (e: UnhandledError) {
    const error = errorStackBySourceMap(e);
    console.error(error);
    alert({
        title: e.type || lang.applicationError,
        message:
            System.OS === 'Android' ? error.stack : e.message + '\n\n*' + error.stack,
    });
};

// SMFApplication.sharedInstance().performActionForShortcutItemShortcutItem = function(shortcutItem) {
//     alert("shortcutItem : " + shortcutItem);
//     return true;
// };

//@ts-ignore
Application.onAppShortcutReceived = function(e) {
    if (e.data) {
        alert("shortcutItem.userInfo: " + JSON.stringify(e.data));
        if (e.data["testbool2"]) {
            router.push('/pages/page2');
        }
    }
};

router.push('/pages/page1');
